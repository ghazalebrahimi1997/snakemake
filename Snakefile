rule prepare:
    input:
        data = "data/data.txt",
        label = "data/SampleCellTypes.txt",
    output:
        dir = directory("prepare_data/")
    shell:
        "python3 data_preparation.py -l {input.label} -d {input.data} -o {output.dir}"


rule dimension_reduction:
    input:
        dir = "prepare_data"
    output:
        output_train = "train_x_compress.csv",
        output_test = "test_x_compress.csv",
        plot = "MSE.png"
    shell:
        "python3 dimension_reduction.py -d {input.dir} -otrain {output.output_train} -otest {output.output_test} -p {output.plot}"

rule classification:
    input:
        reduce_train = "train_x_compress.csv",
        reduce_test = "test_x_compress.csv",
        dir = "prepare_data"
    output:
        output_report = "report.csv",
        plot = "clustering.png"
    shell:
        "python3 classification.py -rtrain {input.reduce_train} -rtest {input.reduce_test} -d {input.dir} -o {output.output_report} -p {output.plot}"
