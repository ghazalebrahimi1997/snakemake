import argparse
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import mean_squared_error
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
import seaborn as sn


def parser_arguments():
    par = argparse.ArgumentParser()
    parser = par.add_argument_group('required arguments')
    parser.add_argument("-rtrain", "--reduce_train", help="directory/path to reduce train data file", required=True)
    parser.add_argument("-rtest", "--reduce_test", help="directory/path to reduce test data file", required=True)
    parser.add_argument("-d", "--dir", help="directory/path to prepare data", required=True)
    parser.add_argument("-o", "--output_report", help="output report filename", required=False)
    parser.add_argument("-p", "--plot", help="plots data", required=True)
    args = par.parse_args()

    return args


def classification(X, Y, args):
    clf = RandomForestClassifier(max_depth=2, random_state=0)
    clf.fit(X, Y['0'])

    return clf


def main():
    args = parser_arguments()

    train_X = pd.read_csv(args.reduce_train)
    test_X = pd.read_csv(args.reduce_test)
    train_y = pd.read_csv(args.dir + "/train_y.csv")
    test_y = pd.read_csv(args.dir + "/test_y.csv")

    clf = classification(train_X, train_y, args)

    y_pred = clf.predict(test_X)
    cnf_matrix = confusion_matrix(test_y['0'], y_pred)
    df_cm = pd.DataFrame(cnf_matrix, train_y.groupby('0').count().index.values.tolist(), train_y.groupby('0').count().index.values.tolist())
    sn.set(font_scale=1.4)
    sn.heatmap(df_cm, annot_kws={"size": 16})

    plt.savefig("clustering.png")
    report = classification_report(test_y['0'], y_pred, output_dict=True)
    df = pd.DataFrame(report).transpose()
    df.to_csv(args.output_report)

if __name__ == '__main__':
    main()
