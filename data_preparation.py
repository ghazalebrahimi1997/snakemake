import argparse
import pandas as pd
from sklearn.model_selection import train_test_split
import os

def parser_arguments():
    par = argparse.ArgumentParser()
    parser = par.add_argument_group('required arguments')
    parser.add_argument("-l", "--label", help="directory/path to label csv file", required=True)
    parser.add_argument("-d", "--data", help="directory/path to data csv file", required=True)
    parser.add_argument("-o", "--output_report", help="output report filename", required=False)
    args = par.parse_args()

    return args


def split_data(gene_expressions, labels, args):
    ratio = 0.25
    train_X, test_X, train_y, test_y = train_test_split(
        gene_expressions,
       labels,
        test_size=ratio,
        random_state=42,
        stratify=labels
    )
    return train_X, test_X, train_y, test_y


def main():
    args = parser_arguments()
    labelfile = args.label
    datafile = args.data
    gene_expressions = pd.read_csv(datafile, header=None)
    labels = pd.read_csv(labelfile, header=None)
    train_X, test_X, train_y, test_y = split_data(gene_expressions, labels, args)
    os.mkdir(args.output_report)
    train_X.reset_index().drop(columns="index").to_csv(args.output_report + "/train_x.csv")
    test_X.reset_index().drop(columns="index").to_csv(args.output_report + "/test_x.csv")
    train_y.reset_index().drop(columns="index").to_csv(args.output_report + "/train_y.csv")
    test_y.reset_index().drop(columns="index").to_csv(args.output_report + "/test_y.csv")


if __name__ == '__main__':
    main()
