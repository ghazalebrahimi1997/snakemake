import argparse
import pandas as pd

import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.metrics import mean_squared_error


def parser_arguments():
    par = argparse.ArgumentParser()
    parser = par.add_argument_group('required arguments')
    parser.add_argument("-d", "--dir", help="directory/path to prepare data", required=True)
    parser.add_argument("-otrain", "--output_train", help="directory/path to reduction train data", required=True)
    parser.add_argument("-otest", "--output_test", help="directory/path to reduction train data", required=True)
    parser.add_argument("-p", "--plot", help="plots data", required=True)
    args = par.parse_args()

    return args


def local_pca(data, args):
    component_size = 30
    pca = PCA(n_components=component_size)
    pca.fit_transform(data)

    return pca


def main():
    args = parser_arguments()
    train_X = pd.read_csv(args.dir + "/train_x.csv")
    test_X = pd.read_csv(args.dir + "/test_x.csv")

    pca = local_pca(test_X, args)
    train_pred = pca.inverse_transform(pca.transform(train_X))
    pd.DataFrame(pca.transform(train_X)).to_csv(args.output_train)

    mean_squared_error_train = mean_squared_error(train_pred.T, train_X.T,  multioutput='raw_values')
    test_pred = pca.inverse_transform(pca.transform(test_X))
    pd.DataFrame(pca.transform(test_X)).to_csv(args.output_test)
    mean_squared_error_test = mean_squared_error(test_pred.T, test_X.T,  multioutput='raw_values')
    plt.boxplot([mean_squared_error_train, mean_squared_error_test], labels=['train','test'], patch_artist=True)
    plt.savefig('MSE.png')


if __name__ == '__main__':
    main()
